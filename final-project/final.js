const urlCategories = `https://www.thecocktaildb.com/api/json/v1/${API_KEY}/list.php?c=list`;
const urlDrinks = `https://www.thecocktaildb.com/api/json/v1/${API_KEY}/filter.php?c=`;
const underageDrinksURL = `https://www.thecocktaildb.com/api/json/v1/${API_KEY}/filter.php?a=Non_Alcoholic`;
const urlDrinkById = `https://www.thecocktaildb.com/api/json/v1/${API_KEY}/lookup.php?i=`


const getDrinksList = function() {
    // get a list of recipie categories and populate the html
    fetch(urlCategories)
        .then(function(response) {
            return response.json();

        })
        .then(function(responseJson) {
            console.log(responseJson);

            for (i = 0; i < responseJson.drinks.length; i++) {
                let displayDrinkCat = responseJson.drinks[i].strCategory;
                let drinkCat = displayDrinkCat.replace(/\s/g, '_');
                console.log(drinkCat);
                document.getElementById('drink-category-list').innerHTML += `<option value=${drinkCat}>${displayDrinkCat}</option>`;

            }

        });
};

const getDrinkDetails = function(drinkId) {
    var urlSpecificDrink = urlDrinkById + drinkId;
    console.log(urlSpecificDrink);
    fetch(urlSpecificDrink)
        .then(function(response) {
            return response.json();

        })
        .then(function(responseJson) {
            var drink = responseJson.drinks[0];
            console.log(drink);
            //this is stupid hacky but is there another way? How?
            document.getElementById('drink-list').innerHTML += `<p>In a ${drink.strGlass}:</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strInstructions}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>Ingredients:</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient1}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient2}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient3}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient4}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient5}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient6}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient7}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient8}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient9}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient10}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient11}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient12}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient13}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient14}</p>`;
            document.getElementById('drink-list').innerHTML += `<p>${drink.strIngredient15}</p>`;
        });
};

// pick a random recipie from the categories suggested, returns from non-alcoholic only if over21 = false

const getDrinks = function(over21, drinkCategory) {
    let drinkCategoryURL = urlDrinks + drinkCategory;
    if (!over21) {
        drinkCategoryURL = underageDrinksURL;
    };
    console.log(drinkCategoryURL);
    fetch(drinkCategoryURL)
        .then(function(response) {
            return response.json();

        })
        .then(function(responseJson) {
            console.log(responseJson);
            let i = Math.floor(Math.random() * responseJson.drinks.length);
            let randomDrink = responseJson.drinks[i].strDrink;
            let randomImage = responseJson.drinks[i].strDrinkThumb;
            let drinkId = responseJson.drinks[i].idDrink;
            document.getElementById('drink-list').innerHTML += `<p>${randomDrink}</p>`;
            document.getElementById('drink-list').innerHTML += `<img id="drink-image" src=${randomImage}></img><br>`;
            let img = document.getElementById('drink-image');
            getDrinkDetails(drinkId);
            if (img.complete) {
                dramaticReveal()
            } else {
                img.addEventListener('load', dramaticReveal)
                img.addEventListener('error', function() {
                    alert('error, immage did not load')
                })
            }

        });
};

// add timer function to reveal drink
const dramaticReveal = function() {
    theOpacity = 0;
    countdownInterval = setInterval(function() {
            if (theOpacity < 1) {
                theOpacity = theOpacity + .05;
                document.getElementById('drink-list').style.opacity = theOpacity;
            } else {
                clearInterval(countdownInterval);
            }

        },
        100);

};


// validates if age is over 21, shows full menu if it is - returns boolean true if so
const checkAge = function(birthdate) {
    let checkDate = new Date(birthdate);
    const now = new Date();
    var chooser = document.getElementById('drink-chooser');
    checkDate.setFullYear(checkDate.getFullYear() + 21);
    if (checkDate <= now) {
        chooser.style.display = '';
        return true;
    } else {
        chooser.style.display = 'none';
        return false;
    };
};

//sets birthdate and name from localstorage

const setMyDetails = function() {
    const savedDate = localStorage.getItem('userBirthDate');
    var birthdate = new Date();
    if (!savedDate) {
        console.log('zoop');
    } else {
        birthdate = new Date(savedDate);
        checkAge(birthdate);
    };
    document.getElementById('birthdate').value = birthdate.toISOString().split('T')[0];
    document.getElementById('user-name').value = localStorage.getItem('userName');

};
//resets the random drink display
const clearTable = function() {
    document.getElementById('drink-list').innerHTML = '';
    document.getElementById('drink-list').style.opacity = 0;
};

//validates and saves details to local storage

const saveDetails = function(birthdate, name) {
    if (document.getElementById('user-name').value.length < 3) {
        document.getElementById('name-error').innerHTML = "Name must have at least three charachters!";
        return;
    } else {
        document.getElementById('name-error').innerHTML = "";
        localStorage.setItem('userBirthDate', birthdate);
        localStorage.setItem('userName', name);
    };
};
//listens for change to datepicker
const ageListener = document.getElementById('birthdate').addEventListener('change', function(e) {
    var birthdate = document.getElementById('birthdate').value;
    checkAge(birthdate);
});

//listens for submit button
const heyListen = document.getElementById('submit').addEventListener('click', function(e) {
    let drinkCategory = document.getElementById('drink-category-list').value;
    let over21 = checkAge(document.getElementById('birthdate').value);
    getDrinks(over21, drinkCategory);
    clearTable();
    e.preventDefault();
});

//listens for save button
const saveListen = document.getElementById('save-details').addEventListener('click', function(e) {
    let birthdate = document.getElementById('birthdate').value;
    let name = document.getElementById('user-name').value;
    saveDetails(birthdate, name);
    e.preventDefault();
});

setMyDetails();
getDrinksList();